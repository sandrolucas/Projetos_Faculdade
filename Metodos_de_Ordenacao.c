/* 
 * File:   Metodos_de_Ordenacao.c
 * Author: Sandro_Ferreira
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Biblioteca para trabalhar com tempo
#include <locale.h> //Biblioteca para acentuação

int main(int argc, char** argv) {
    setlocale(LC_ALL, "Portuguese");

    int tamanho_vator;
    time_t t; //Armazena valores de tempo
    clock_t ti; //Retorna o número de pulsos de clock decorridos desde o inicio do programa

    // Gera os numeros aleatórios
    printf("Informe o tamanho do vetor:  ");
    scanf("%d", &tamanho_vator);

    int vetor[tamanho_vator]; //Vetor 1 - auxiliar
    int vetor_original[tamanho_vator]; //Vetor afu
    int itera = 0;
    srand((unsigned) time(&t)); //
    for (int i = 0; i < tamanho_vator; i++) {
        vetor[i] = (rand() % 1000000); //aleatório
        vetor_original[i] = vetor[i]; //atribui valor
    }

    //2.3. Listar os números gerados na tela (primeiros e últimos).
    void Posicoes() {
        printf("Pos:[0] Valor:%d, Pos:[1] %d, Pos:[2] %d, ... "
                "Pos:[%d] Valor:%d, Pos:[%d] Valor:%d, Pos:[%d] Valor:%d.\t",
                vetor[0], vetor[1], vetor[1],
                tamanho_vator - 3, vetor[tamanho_vator - 3],
                tamanho_vator - 2, vetor[tamanho_vator - 2],
                tamanho_vator - 1, vetor[tamanho_vator - 1]);
        printf("\nIterações: %d", itera);
    }

    //2.4. Informar a hora com hora : minuto segundo : milisegundo na tela.

    void iniciaRelogio() { //
        ti = clock();
    }

    void paraRelogio() {
        ti = clock() - ti;
        double tempo_levado = ((double) ti) / CLOCKS_PER_SEC; // converte em segundos
        printf("\nTempo total em segundos: %f \n", tempo_levado);
    }

    /*
     * Função para Bubble Sort
     * https://upload.wikimedia.org/wikipedia/commons/3/37/Bubble_sort_animation.gif
     * A ideia é percorrer o vector diversas vezes, e a cada passagem fazer flutuar para o topo o maior elemento da sequência.
     */
    void bubbleSort() {
        itera = 0;
        int c, p = 0;
        while (c != -1) {
            for (int i = 0; i < tamanho_vator; i++) {
                if (vetor[i] > vetor[i + 1] && i + 1 < tamanho_vator) {
                    p = vetor[i + 1];
                    vetor[i + 1] = vetor[i];
                    vetor[i] = p;
                    c++;
                    itera++;
                }
                itera++;
            }
            c = (c == 0) ? -1 : 0;
        }
    }

    /*
     * Função para Sequential Sort
     * A ideia é passar sempre o menor valor do vetor para a primeira posição depois o de segundo menor valor 
     * para a segunda posição e assim sucessivamente.
     */
    void SeqSort() {

        itera = 0;

        int menor, menor_posicao = 0;
        for (int i = 0; i < tamanho_vator; i++) {

            menor_posicao = i;
            menor = vetor[menor_posicao];

            for (int j = i; j < tamanho_vator; j++) {
                if (vetor[j] < menor) {
                    menor_posicao = j;
                    menor = vetor[j];
                    itera++;
                }
                itera++;
            }
            menor = vetor[menor_posicao];
            vetor[menor_posicao] = vetor[i];
            vetor[i] = menor;
            itera++;
        }
    }

    /*
     * Função para Insertion Sort
     * https://upload.wikimedia.org/wikipedia/commons/2/25/Insertion_sort_animation.gif
     * A ideia é construir uma matriz final com um elemento de cada vez, uma inserção por vez.
     */
    void InsertSort() {
        itera = 0;
        for (int i = 0; i < tamanho_vator; i++) {
            int j = i;
            while (j > 0 && vetor[j - 1] > vetor[j]) {
                int temp = vetor[j];
                vetor[j] = vetor[j - 1];
                vetor[j - 1] = temp;
                j--;
                itera++;
            }
            itera++;
        }
    }

    /*
     * Função para Merge Sort
     * https://upload.wikimedia.org/wikipedia/commons/c/c5/Merge_sort_animation2.gif
     * A ideia é criar uma sequência ordenada a partir de duas outras também ordenadas. Dividir e Conquistar
     */
    void merge(int* arr, int* left_arr, int left_count, int* right_arr, int right_count) {
        int i = 0, j = 0, k = 0;

        while (i < left_count && j < right_count) {
            if (left_arr[i] < right_arr[j]) {
                arr[k++] = left_arr[i++];
                itera++;
            } else {
                arr[k++] = right_arr[j++];
                itera++;
            }
            itera++;
        }
        while (i < left_count) {
            arr[k++] = left_arr[i++];
            itera++;
        }

        while (j < right_count) {
            arr[k++] = right_arr[j++];
            itera++;
        }
    }

    void MergeS(int arr[], int size) {
        int mid, i, *L, *right_arr;
        if (size < 2) return;
        mid = size / 2;

        L = (int*) malloc(mid * sizeof (int));
        right_arr = (int*) malloc((size - mid) * sizeof (int));

        for (i = 0; i < mid; i++) {
            L[i] = arr[i];
            itera++;
        }
        for (i = mid; i < size; i++) {
            right_arr[i - mid] = arr[i];
            itera++;
        }

        MergeS(L, mid);
        MergeS(right_arr, size - mid);
        merge(arr, L, mid, right_arr, size - mid);
        free(L);
        free(right_arr);
    }

    void MergeSort() {
        itera = 0;
        MergeS(vetor, tamanho_vator);
    }

    /*
     * Função para Quick Sort
     * https://upload.wikimedia.org/wikipedia/commons/6/6a/Sorting_quicksort_anim.gif
     * A estratégia consiste em rearranjar as chaves de modo que as chaves "menores" precedam as chaves "maiores". 
     * Em seguida o quicksort ordena as duas sublistas de chaves menores e maiores recursivamente até que a 
     * lista completa se encontre ordenada.
     */
    void quickS(int arr[], int primeiro_indice, int ultimo_indice) {

        int indicePivo, temp, index_a, index_b;

        if (primeiro_indice < ultimo_indice) {

            indicePivo = primeiro_indice;
            index_a = primeiro_indice;
            index_b = ultimo_indice;

            while (index_a < index_b) {
                while (arr[index_a] <= arr[indicePivo] && index_a < ultimo_indice) {
                    index_a++;
                    itera++;
                }
                while (arr[index_b] > arr[indicePivo]) {
                    index_b--;
                    itera++;
                }

                if (index_a < index_b) {

                    temp = arr[index_a];
                    arr[index_a] = arr[index_b];
                    arr[index_b] = temp;
                    itera++;
                }
                itera++;
            }
            temp = arr[indicePivo];
            arr[indicePivo] = arr[index_b];
            arr[index_b] = temp;
            quickS(arr, primeiro_indice, index_b - 1);
            quickS(arr, index_b + 1, ultimo_indice);
        }
    }

    void QuickSort() {

        itera = 0;
        quickS(vetor, 0, tamanho_vator - 1);
    }

    /*
     * Função para Heap Sort
     * https://upload.wikimedia.org/wikipedia/commons/1/1b/Sorting_heapsort_anim.gif
     * É um algoritimo que utiliza uma estrutura de dados conhecida como Heap binário para buscar o próximo
     * item a ser relacionado.
     * Heap: E a organização de memória mais flexível que permite o uso de qualquer área lógica disponível.
     */

    void isHeap(int arr[], int i, int tamanho_pilha) {

        int l, r, largest, temp;
        l = 2 * i;
        r = 2 * i + 1;
        if (l <= tamanho_pilha && arr[l] > arr[i]) {
            largest = l;
            itera++;
        } else {
            largest = i;
            itera++;
        }
        if (r <= tamanho_pilha && arr[r] > arr[largest]) {
            largest = r;
            itera++;
        }
        if (largest != i) {
            temp = arr[i];
            arr[i] = arr[largest];
            arr[largest] = temp;
            isHeap(arr, largest, tamanho_pilha);
            itera++;
        }
    }

    void fazHeap(int arr[], int n) {
        int i, tamanho_pilha;
        tamanho_pilha = tamanho_vator - 1;
        for (i = (tamanho_vator / 2); i >= 0; i--) {
            isHeap(arr, i, tamanho_pilha);
            itera++;
        }
    }

    void HeapSort() {

        fazHeap(vetor, tamanho_vator);
        int tamanho_pilha, i, temp;
        tamanho_pilha = tamanho_vator - 1;

        for (i = tamanho_pilha; i >= 0; i--) {
            temp = vetor[0];
            vetor[0] = vetor[tamanho_pilha];
            vetor[tamanho_pilha] = temp;
            tamanho_pilha--;
            isHeap(vetor, 0, tamanho_pilha);
            itera++;
        }
    }

    /*Menu Principal*/
    Posicoes();
    int opcao = 0;
    printf("\n\nInforme o número do método de ordenação desejado:\n");
    printf("1. Bubble Sort\n");
    printf("2. Sequencial Sort\n");
    printf("3. Insertion Sort\n");
    printf("4. Merge Sort\n");
    printf("5. Quick Sort\n");
    printf("6. Heap Sort\n");

    printf("Digite a opcao desejada: ");
    scanf("%d", &opcao);

    switch (opcao) {
        case 1:
            iniciaRelogio();
            bubbleSort();
            Posicoes();
            paraRelogio();
            break;

        case 2:
            iniciaRelogio();
            SeqSort();
            Posicoes();
            paraRelogio();
            break;

        case 3:
            iniciaRelogio();
            InsertSort();
            Posicoes();
            paraRelogio();
            break;

        case 4:
            iniciaRelogio();
            MergeSort();
            Posicoes();
            paraRelogio();
            break;

        case 5:
            iniciaRelogio();
            QuickSort();
            Posicoes();
            paraRelogio();
            break;

        case 6:
            iniciaRelogio();
            HeapSort();
            Posicoes();
            paraRelogio();
            break;

        default:
            printf("Você selecionou uma opção que não existe no menu. Tente novamente.");
            return 0;
            break;
    }
}